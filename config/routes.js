module.exports.routes = {

  /*************************************************************
   * Server-rendered HTML Pages                                *
   *************************************************************/
  'GET /': 'pageController.showHomePage',
  
  
  // 'GET /': { 
  //   view: 'homepage',
  //   locals: {
  //     me: {
  //       id: null,
  //     // gravatarURL: 'http://www.gravatar.com/avatar/ef3eac6c71fdf24b13db12d8ff8d1264?',
  //     // email: 'sailsinaction@gmail.com'
  //     }
  //   }
  // },

  'PUT /login': 'UserController.login',
  
  'GET /videos': 'pageController.showVideosPage',
  
  'GET /profile': 'pageController.showProfilePage',

  // 'GET /profile': {
  //   view: 'profile',
  //     locals: {
  //       me: {
  //         id: 1,
  //         gravatarURL: 'http://www.gravatar.com/avatar/ef3eac6c71fdf24b13db12d8ff8d1264?',
  //         email: 'sailsinaction@gmail.com',
  //         username: 'sails-in-action'
  //     }
  //   }
  // },
  
  'GET /edit-profile': 'pageController.showEditProfilePage',
  
  // 'GET /edit-profile': {
  //   view: 'edit-profile',
  //   locals: {
  //     me: {
  //       id: 1,
  //       gravatarURL: 'http://www.gravatar.com/avatar/ef3eac6c71fdf24b13db12d8ff8d1264?',
  //       email: 'sailsinaction@gmail.com',
  //       username: 'sails-in-action'
  //     }
  //   }
  // },
  'GET /profile': 'pageController.showProfilePage',
  
  // 'GET /signup': {
  //   view: 'signup',
  //   locals: {
  //     me: {
  //       id: null,
  //       gravatarURL: 'http://www.gravatar.com/avatar/ef3eac6c71fdf24b13db12d8ff8d1264?',
  //       email: 'sailsinaction@gmail.com'
  //     }
  //   }
  // },
  'GET /restore-profile': {
    view: 'restore-profile',
    locals: {
      me: {
        id: null,
        gravatarURL: 'http://www.gravatar.com/avatar/ef3eac6c71fdf24b13db12d8ff8d1264?',
        email: 'sailsinaction@gmail.com'
      }
    }
  },
  'GET /administration': {
    view: 'adminUsers',
    locals: {
      me: {
        id: 1,
        gravatarURL: 'http://www.gravatar.com/avatar/ef3eac6c71fdf24b13db12d8ff8d1264?',
        email: 'sailsinaction@gmail.com',
      }
    }
  }
};